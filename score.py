class score(object):

    def __init__(self):
        try:
            self.score = 0
            with open("score", "r") as self.scfile:
                self.bestScore = int(self.scfile.readline().replace("\n", ""))
        except:
            self.score = 0
            self.bestScore = 0
        with open("score", "w") as self.scfile:
            self.scfile.write("0")

    def new(self, score):
        self.score += score
        with open("score", "w") as self.scfile:
            if self.score > self.bestScore:
                self.scfile.write(str(self.score))
                self.bestScore = self.score
                print "new best score", self.bestScore
            else:
                self.scfile.write(str(self.bestScore))

    def remove(self):
        self.bestScore = 0
        with open("score", "w") as self.scfile:
            self.scfile.write("0")

    def best(self):
        return self.bestScore

    def actual(self):
        return self.score
