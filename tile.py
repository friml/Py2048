import pygame

class tile(object):
    
    def __init__(self,power,pos,score):
        
        self.score = score   
        self.xpos = pos[0]*100
        self.ypos = pos[1]*100
        self.size = (100,100)
        self.power = power
        self.nowUpdated = False
        self.pics = [(0,0,0),pygame.image.load("img/1.bmp"),
                             pygame.image.load("img/2.bmp"),
                             pygame.image.load("img/3.bmp"),
                             pygame.image.load("img/4.bmp"),
                             pygame.image.load("img/5.bmp"),
                             pygame.image.load("img/6.bmp"),
                             pygame.image.load("img/7.bmp"),
                             pygame.image.load("img/8.bmp"),
                             pygame.image.load("img/9.bmp"),
                             pygame.image.load("img/10.bmp"),
                             pygame.image.load("img/11.bmp"),
                             pygame.image.load("img/12.bmp"),          
                      ]
        self.movedown = False
        self.moveup = False
        self.moveleft = False
        self.moveright = False
        
    def update(self,matrix):
        
        self.moved = []
    
        if self.movedown and self.ypos >= 300:
            self.movedown = False
        if self.moveup and self.ypos <= 0:
            self.moveup = False
        if self.moveleft and self.xpos <= 0:
            self.moveleft = False
        if self.moveright and self.xpos >= 300:
            self.moveright = False
        
        if self.movedown and matrix[(self.xpos/100 , (self.ypos/100)+1)] is not None:
            if matrix[(self.xpos/100 , (self.ypos/100)+1)].nowUpdated:
                self.movedown = False
            elif matrix[(self.xpos/100 , (self.ypos/100)+1)].power == self.power:
                self.power += 1
                self.nowUpdated = True
                self.moved.append(True)
                self.score.new(2**self.power)
                matrix[(self.xpos/100 , (self.ypos/100)+1)] = None
                self.ypos += 100
            self.movedown = False
            
        if self.moveup and matrix[(self.xpos/100 , (self.ypos/100)-1)] is not None:
            if matrix[(self.xpos/100 , (self.ypos/100)-1)].nowUpdated:
                self.movedown = False
            elif matrix[(self.xpos/100 , (self.ypos/100)-1)].power == self.power:
                self.power += 1
                self.nowUpdated = True
                self.moved.append(True)
                self.score.new(2**self.power)
                matrix[(self.xpos/100 , (self.ypos/100)+-1)] = None
                self.ypos -= 100
            self.moveup = False
            
        if self.moveleft and matrix[((self.xpos/100)-1 , (self.ypos/100))] is not None:
            if matrix[((self.xpos/100)-1 , self.ypos/100)].nowUpdated:
                self.movedown = False
            elif matrix[((self.xpos/100)-1 , self.ypos/100)].power == self.power:
                self.power +=1
                self.nowUpdated = True
                self.moved.append(True)
                self.score.new(2**self.power)
                matrix[((self .xpos/100)-1 , self.ypos/100)] = None
                self.xpos -= 100
            self.moveleft = False
            
        if self.moveright and matrix[((self.xpos/100)+1 , (self.ypos/100))] is not None:
            if matrix[((self.xpos/100)+1 , self.ypos/100)].nowUpdated:
                self.movedown = False
            elif matrix[((self.xpos/100)+1 , self.ypos/100)].power == self.power:
                self.power += 1
                self.nowUpdated = True
                self.moved.append(True)
                self.score.new(2**self.power)
                matrix[((self.xpos/100)+1 , self.ypos/100)] = None
                self.xpos += 100
            self.moveright = False
                   
        if self.movedown == True:
            self.ypos += 100
        if self.moveup == True:
            self.ypos -= 100
        if self.moveleft == True:
            self.xpos -= 100
        if self.moveright == True:
            self.xpos += 100 
                    
        if self.movedown or self.moveup or self.moveleft or self.moveright or len(self.moved) > 0:
            return True
        
    def draw(self,surface):
#        pygame.draw.rect(surface, 0, pygame.Rect((self.xpos,self.ypos), self.size))
        surface.blit(self.pics[self.power],self.position(1))
    
    def position(self,modulator):
        return (self.xpos/modulator,self.ypos/modulator)

