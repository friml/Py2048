import pygame
import sys
import tile
import dicMatrix
import random
import score
from pygame.locals import *


pygame.init()


class main(object):

    def __init__(self):

        # Setting variables
        self.display = pygame.display.set_mode((480, 640))
        self.surface = pygame.Surface((400, 400))

        self.basicfont = pygame.font.SysFont("helvetica", 26, True)

        pygame.display.set_caption("Python 2048")
        self.map = dicMatrix.dicMatrix(3, 3)

        self.score = score.score()
        self.display.blit(pygame.image.load("img/bgDisp.bmp"), (0, 0))
        self.surface.blit(pygame.image.load("img/bg.bmp"), (0, 0))

        self.run = True
        self.wonYet = False

        # Generating first two tiles

        self.randomtile()
        self.refreshMaps()
        self.randomtile()
        self.refreshMaps()
        self.flip()

        # Main loop
        while self.run:
            for event in pygame.event.get():

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

                if event.type == KEYUP:

                    self.refreshMaps()

                    if event.key == ord('r'):
                        self.run = False

                    if event.key == K_F5:
                        self.display.blit(
                            pygame.image.load("img/bgDisp.bmp"), (0, 0))
                        self.score.remove()

                    if event.key == ord('h'):
                        self.help = (pygame.image.load("img/help.bmp"))
                        self.help.set_alpha(200)
                        self.popup(self.help)

                    if event.key == K_LEFT or event.key == ord('a'):

                        for key in self.filledKeys.keys():
                            self.filledKeys[key].moveleft = True
                        if self.possmov(False):
                            self.randomtile()

                    if event.key == K_RIGHT or event.key == ord('d'):

                        for key in self.filledKeys.keys():
                            self.filledKeys[key].moveright = True
                        if self.possmov(True):
                            self.randomtile()

                    if event.key == K_UP or event.key == ord('w'):

                        for key in self.filledKeys.keys():
                            self.filledKeys[key].moveup = True
                        if self.possmov(False):
                            self.randomtile()

                    if event.key == K_DOWN or event.key == ord('s'):

                        for key in self.filledKeys.keys():
                            self.filledKeys[key].movedown = True
                        if self.possmov(True):
                            self.randomtile()

                    self.flip()

                if not self.wonYet:
                    for key in self.filledKeys:
                        if self.map.matrix[key].power == 11:
                            self.wonYet = True
                            self.won = (pygame.image.load("img/won.bmp"))
                            self.won.set_alpha(200)
                            self.popup(self.won)

    def randomtile(self):

        self.refreshMaps()
        if random.randint(1, 10) == 10:
            self.power = 2
        else:
            self.power = 1

        randmax = random.randint(0, len(self.emptyKeys.keys())-1)
        self.map.matrix[
            self.emptyKeys.keys()[randmax]] = tile.tile(
            self.power,
            self.emptyKeys.keys()[randmax],
            self.score)
        self.map.matrix[self.emptyKeys.keys()[randmax]].draw(self.surface)

    def refreshMaps(self):

        self.emptyKeys = {}
        self.filledKeys = {}
        for key in self.map.matrix.keys():

            if self.map.matrix[key] is None:
                self.emptyKeys[key] = self.map.matrix[key]
            else:
                self.filledKeys[key] = self.map.matrix[key]

    def possmov(self, descending):

        possmoves = []
        self.surface.blit(pygame.image.load("img/bg.bmp"), (0, 0))
        for key in sorted(self.filledKeys, reverse=descending):

            if self.filledKeys[key].update(self.map.matrix):
                possmoves.append(True)
                self.filledKeys[key].update(self.map.matrix)
                self.filledKeys[key].update(self.map.matrix)

            self.filledKeys[key].draw(self.surface)
            self.map.swap(self.filledKeys[key].position(100), key)
            self.refreshMaps()

        pygame.display.flip()

        for key in self.filledKeys:
            self.filledKeys[key].nowUpdated = False

        if len(possmoves) > 0:
            return True

    def flip(self):
        self.display.blit(self.surface, (40, 210))

        self.SCORE = self.basicfont.render(
            " " + str(self.score.actual()) + " ", True, (255, 255, 255),
            (187, 173, 160))
        self.SCORErect = self.SCORE.get_rect(center=(267, 80))
        self.display.blit(self.SCORE, self.SCORErect)
        self.SCORErect = None

        self.BEST = self.basicfont.render(
            " " + str(self.score.best()) + " ", True, (255, 255, 255),
            (187, 173, 160))
        self.BESTrect = self.BEST.get_rect(center=(388, 80))
        self.display.blit(self.BEST, self.BESTrect)

        pygame.display.flip()

    def popup(self, surface):
        self.display.blit(surface, (0, 0))
        pygame.display.flip()
        self.popupshow = True
        while self.popupshow:
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
                if event.type == KEYUP:
                    self.display.blit(
                        pygame.image.load("img/bgDisp.bmp"), (0, 0))
                    self.flip()
                    self.popupshow = False

while True:
    main()
