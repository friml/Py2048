class dicMatrix(object):
    def __init__(self,x=1,y=1):
        self.matrix = {}
        for x in range(0,x+1):
            for y in range(0,y+1):
                self.matrix[(x,y)] = None
                
    def swap(self,key1,key2):
        if key1 != key2:
            self.key1backup = self.matrix.pop(key1)
            self.key2backup = self.matrix.pop(key2)
        
            self.matrix[key1] = self.key2backup 
            self.matrix[key2] = self.key1backup