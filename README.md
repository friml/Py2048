# Py2048
2048 game written in Python

##Installation

Before installation, please check you have python 2.7 and pygame installed.

```
https://github.com/frimdo/Py2048
```

##Launching

You can launch game doubleclicking on "main.py", or from console

```
python2.7 ./main.py
```

##Screenshot

![screenshot](https://github.com/frimdo/Py2048/blob/master/screenshot.jpg)
